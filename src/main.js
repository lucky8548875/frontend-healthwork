import Vue from 'vue'
import App from './App.vue'
import './registerServiceWorker'
import router from './router'
import store from './store'
import './style/main.sass'
import {MenuIcon, ArrowLeftIcon, EyeIcon, CheckIcon, LogOutIcon, AlertTriangleIcon, CodeIcon, ChevronDownIcon, ChevronUpIcon} from 'vue-feather-icons'
import Tabs from './components/Tabs'

Vue.component('MenuIcon', MenuIcon)
Vue.component('ArrowLeftIcon', ArrowLeftIcon)
Vue.component('LogOutIcon', LogOutIcon)
Vue.component('EyeIcon', EyeIcon)
Vue.component('Tabs', Tabs)
Vue.component('CheckIcon', CheckIcon)
Vue.component('AlertTriangleIcon', AlertTriangleIcon)
Vue.component('ChevronDownIcon',ChevronDownIcon)
Vue.component('ChevronUpIcon', ChevronUpIcon)
Vue.component('CodeIcon', CodeIcon)

Vue.config.productionTip = false

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
